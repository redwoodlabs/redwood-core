from django.apps import AppConfig


class RedwoodCoreConfig(AppConfig):
    name = 'redwood_core'

    def ready(self):
        import redwood_core.signals
