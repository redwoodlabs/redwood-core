from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.core.mail import send_mail

from django.conf import settings

@receiver(post_save, sender=User)
def send_email_to_admin(sender, instance, created, **kwargs):
    if created:
        send_mail(
            'New %s User: %s' % (settings.SITE_NAME, instance.get_full_name()),
            '%(name)s (%(email)s) just joined %(site)s.' % {'name': instance.get_full_name(), 'email': instance.email, 'site': settings.SITE_NAME},
            'noreply@%s' % settings.SITE_DOMAIN,
            ['brendenmulligan+%s@gmail.com' % settings.PROJECT_NAME],
            fail_silently=False,
        )
