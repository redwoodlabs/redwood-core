from django.urls import path
from django.views.generic.base import RedirectView
from django.views.generic import TemplateView

urlpatterns = [

    path('login/', RedirectView.as_view(pattern_name='account_login', permanent=True)),
    path('logout/', RedirectView.as_view(pattern_name='account_logout', permanent=True)),
    path('signup/', RedirectView.as_view(pattern_name='account_signup', permanent=True)),

    path('privacy/', TemplateView.as_view(template_name='marketing/privacy.html'), name='privacy_page'),
    path('terms/', TemplateView.as_view(template_name='marketing/terms.html'), name='terms_page'),

]
